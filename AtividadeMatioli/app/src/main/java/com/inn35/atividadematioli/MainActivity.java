package com.inn35.atividadematioli;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == 0){
            Toast.makeText(getApplicationContext(),"Perfil atualizado com sucesso!",  Toast.LENGTH_LONG).show();
        } else if (resultCode == -1) {
            Toast.makeText(getApplicationContext(), "Operação Cancelada", Toast.LENGTH_LONG).show();
        }
    }


    public void openActive1(View view) {
        Intent it = new Intent(MainActivity.this, CadastraActive.class);
        startActivity(it);
        startActivityForResult(it, 1);

    }
}
