package com.inn35.atividadematioli;

import android.os.Bundle;
import android.app.Activity;
import android.view.View;

public class CadastraActive extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastra_active);
    }

    public void finishThisRight(View view) {
        setResult(0);
        finish();
    }
    public void finishThis(View view) {
        setResult(-1);
        finish();
    }
}
